/**
 * Horizontal Menu
 */
import React, { Component, Fragment } from 'react';
import { Dropdown, DropdownToggle, DropdownMenu } from 'reactstrap';
import { Link } from 'react-router-dom';
import { NavLink } from 'react-router-dom';

import IntlMessages from '../../util/IntlMessages';

import navLinks from './NavLinks';

import NavMenuItem from './NavMenuItem';

import uassessLogo from '../../assets/img/uassessLogo.png'

class HorizontalMenu extends Component {
    state = {
        userDropdownMenu: false,
        isSupportModal: false
    }

    /**
     * Logout User
     */
    logoutUser() {
        this.props.logoutUserFromFirebase();
    }
    /**
     * Toggle User Dropdown Menu
     */
    toggleUserDropdownMenu() {
        this.setState({ userDropdownMenu: !this.state.userDropdownMenu });
    }
    render() {
        return (
            <div className="horizontal-menu">
                <img src={uassessLogo}/>
                <ul className="list-unstyled nav">
                    <li className="nav-item">
                        <a href="javascript:void(0);" className="nav-link">
                            <i className="ti-home"></i>
                            <span className="menu-title"><IntlMessages id="sidebar.insigths" /></span>
                        </a>
                        <ul className="list-unstyled sub-menu">
                            {/* {navLinks.category1.map((menu, key) => (
                                <NavMenuItem
                                    menu={menu}
                                    key={key}
                                />
                            ))} */}
                        </ul>
                    </li>
                    <li className="nav-item">
                        <a href="javascript:void(0);" className="nav-link">
                            <i className="ti-package"></i>
                            <span className="menu-title"><IntlMessages id="sidebar.usage" /></span>
                        </a>
                        <ul className="list-unstyled sub-menu">
                            {/* {navLinks.category2.map((menu, key) => (
                                <NavMenuItem
                                    menu={menu}
                                    key={key}
                                />
                            ))} */}
                        </ul>
                    </li>
                    <li>
                        <div className="sidebar-user-block media">
                            <div className="user-profile">
                                <img src={require('../../assets/img/user-7.jpg')} alt="user profile" className="img-fluid rounded-circle" width="40" height="40" />
                            </div>
                            <Dropdown isOpen={this.state.userDropdownMenu} toggle={() => this.toggleUserDropdownMenu()} className="rct-dropdown media-body pt-10">
                                <DropdownToggle nav>
                                    Lucile Beck
                                    <i className="ti-angle-down pull-right"></i>
                                </DropdownToggle>
                                <DropdownMenu>
                                    <ul className="list-unstyled mb-0">
                                        <li className="border-top">
                                            <a href="javascript:void(0)" onClick={() => this.logoutUser()}>
                                                <i className="ti ti-power-off"></i>
                                                <IntlMessages id="widgets.logOut" />
                                            </a>
                                        </li>
                                    </ul>
                                </DropdownMenu>
                            </Dropdown>
                        </div>
                    </li>
                </ul>
            </div>
        );
    }
}

export default HorizontalMenu;
