/**
 * App.js Layout Start Here
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';
import { MuiThemeProvider } from 'material-ui/styles';
import { IntlProvider } from 'react-intl';
import { Redirect, Route } from 'react-router-dom';
import { NotificationContainer } from 'react-notifications';

//Horizontal Layout
import HorizontalLayout from './HorizontalLayout';

//Main App
import MainApp from '../routes';

// app signin
import AppSignIn from './SigninFirebase';
import AppSignUp from './SignupFirebase';

// App locale
import AppLocale from '../lang';

// themes
import primaryTheme from './themes/primaryTheme';
import darkTheme from './themes/darkTheme';
import secondaryTheme from './themes/secondaryTheme';
import warningTheme from './themes/warningTheme';
import dangerTheme from './themes/dangerTheme';
import infoTheme from './themes/infoTheme';
import successTheme from './themes/successTheme';
import purpleTheme from './themes/purpleTheme';

// async components
import {
  AsyncSessionLoginComponent,
  AsyncSessionRegisterComponent,
  AsyncSessionLockScreenComponent,
  AsyncSessionForgotPasswordComponent,
  AsyncSessionPage404Component,
  AsyncSessionPage500Component,
  AsyncTermsConditionComponent
} from '../components/AsyncComponent/AsyncComponent';

/**
 * Initial Path To Check Whether User Is Logged In Or Not
 */
const InitialPath = ({ component: Component, ...rest, authUser }) =>
  <Route
    {...rest}
    render={props =>
      authUser
        ? <Component {...props} />
        : <Redirect
          to={{
            pathname: '/signin',
            state: { from: props.location }
          }}
        />}
  />;

class App extends Component {
  render() {
    const { locale, darkMode, rtlLayout, activeTheme } = this.props.settings;
    if (this.props.location.pathname === '/') {
      if (this.props.user === null) {
        return (<Redirect to={'/signin'} />);
      } else {
        return (<Redirect to={'/app/admin-dashboard'} />);
      }
    }
    const currentAppLocale = AppLocale[locale.locale];

    // theme changes

    let theme = '';
    switch (activeTheme.id) {
      case 1:
        theme = primaryTheme
        break;
      case 2:
        theme = secondaryTheme
        break;
      case 3:
        theme = warningTheme
        break;
      case 4:
        theme = infoTheme
        break;
      case 5:
        theme = dangerTheme
        break;
      case 6:
        theme = successTheme
        break;
      default:
        break;
    }

    if (darkMode) {
      theme = darkTheme
    }

    if (rtlLayout) {
      theme.direction = 'rtl'
    } else {
      theme.direction = 'ltr'
    }
    return (
      <MuiThemeProvider theme={theme}>
        <IntlProvider
          locale={currentAppLocale.locale}
          messages={currentAppLocale.messages}
        >
          <React.Fragment>
            <NotificationContainer />
            <InitialPath path={`${this.props.match.url}app`} authUser={this.props.user} component={MainApp} />
            <Route path="/horizontal" component={HorizontalLayout} />
            <Route path="/signin" component={AppSignIn} />
            <Route path="/signup" component={AppSignUp} />
            <Route path="/session/login" component={AsyncSessionLoginComponent} />
            <Route path="/session/register" component={AsyncSessionRegisterComponent} />
            <Route path="/session/lock-screen" component={AsyncSessionLockScreenComponent} />
            <Route path="/session/forgot-password" component={AsyncSessionForgotPasswordComponent} />
            <Route path="/session/404" component={AsyncSessionPage404Component} />
            <Route path="/session/500" component={AsyncSessionPage500Component} />
            <Route path="/terms-condition" component={AsyncTermsConditionComponent} />
          </React.Fragment>
        </IntlProvider>
      </MuiThemeProvider>
    );
  }
}

// map state to props
const mapStateToProps = ({ settings, authUser }) => {
  const { user } = authUser;
  return { settings, user };
};

export default connect(mapStateToProps)(App);
