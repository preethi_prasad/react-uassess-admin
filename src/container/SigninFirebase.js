/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from 'material-ui/Button';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import { Link } from 'react-router-dom';
import { Form, FormGroup, Input, Label } from 'reactstrap';
import { LinearProgress } from 'material-ui/Progress';
import QueueAnim from 'rc-queue-anim';
import uassess from '../assets/img/uassess.png';
import axios from 'axios';

// components
import SessionSlider from '../components/Widgets/SessionSlider';

// app config
import AppConfig from '../constants/AppConfig';

// redux action
import {
  signinUserInFirebase,
  signinUserWithFacebook,
  signinUserWithGoogle,
  signinUserWithGithub,
  signinUserWithTwitter
} from '../actions';

class Signin extends Component {

  state = {
    email: 'demo@example.com',
    password: 'test#123'
  }

  /**
   * On User Login
   */
  onUserLogin() {
    if (this.state.email !== '' && this.state.password !== '') {
      var responseData = {
        'email':this.state.email,
        'password': this.state.password,
        'role':document.getElementById('exampleSelect').value
      }
      //console.log('option',document.getElementById('exampleSelect').value)
      axios.post('http://localhost/nitishgulati-web-3418dd34f200/public/adminLogin', JSON.stringify(responseData), { 'headers': { 'Content-Type': 'application/json' } }, ).then((response) => {
        console.log('response',response.data)
      });
      //this.props.signinUserInFirebase(this.state, this.props.history);
    }
  }

  /**
   * On User Sign Up
   */
  onUserSignUp() {
    this.props.history.push('/signup');
  }

  render() {
    const { email, password } = this.state;
    const { loading } = this.props;
    return (
      <QueueAnim type="bottom" duration={2000}>
        <div style={{ height: '100vh', backgroundColor: '#fff' }}
        //  className="rct-session-wrapper"
        >
          {loading &&
            <LinearProgress />
          }
          {/* <AppBar position="static" className="session-header">
            <Toolbar>
              <div className="container">
                <div className="d-flex justify-content-between">
                  <div className="session-logo">
                    <Link to="/">
                      <img src={AppConfig.appLogo} alt="session-logo" className="img-fluid" width="110" height="35" />
                    </Link>
                  </div>
                  <div>
                    <a className="mr-15" onClick={() => this.onUserSignUp()}>Create New account?</a>
                    <Button variant="raised" color="primary" className="btn-lg circle-btn-xs" onClick={() => this.onUserSignUp()}>Sign Up</Button>
                  </div>
                </div>
              </div>
            </Toolbar>
          </AppBar> */}
          <div className="session-inner-wrapper">
            {/* <div className="container"> */}
            <div className="row row-eq-height">
              <div className="col-sm-6 col-md-6 col-lg-6" style={{ padding: '7em' }}>
                <div className="session-body text-center">
                  <div className="session-head mb-30">
                    <h2 className="login-text">LOGIN</h2>
                    {/* <p className="mb-0">Most powerful ReactJS admin panel</p> */}
                  </div>
                  <Form id="loginForm">
                    <FormGroup className="has-wrapper">
                      <Label className="label">Employee ID</Label>
                      <Input type="mail" value={email} name="user-mail" id="user-mail" className="has-input input-lg" placeholder="Enter Email Address" onChange={(event) => this.setState({ email: event.target.value })} />
                      {/* <span className="has-icon"><i className="ti-email"></i></span> */}
                    </FormGroup>

                    <FormGroup className="has-wrapper">
                      <Label for="exampleSelect" className="label">Role</Label>
                      <Input type="select" name="select" id="exampleSelect" className="role-dropdown">
                        <option value='companyAdmin'>Company Admin</option>
                      </Input>
                    </FormGroup>

                    <FormGroup className="has-wrapper">
                      <Label className="label">Password</Label>
                      <Input value={password} type="Password" name="user-pwd" id="pwd" className="has-input input-lg" placeholder="Password" onChange={(event) => this.setState({ password: event.target.value })} />
                      {/* <span className="has-icon"><i className="ti-lock"></i></span> */}
                    </FormGroup>
                    <FormGroup className="mb-15" style={{textAlign: "center"}}>
                      <Button
                        className="btn-success text-white btn-lg circle-btn-sm login-btn"
                        variant="raised"
                        onClick={() => this.onUserLogin()}>
                        Let me in
                            </Button>
                    </FormGroup>
                  </Form>
                  {/* <p className="mb-20">or sign in with</p> */}
                  {/* <Button variant="fab" className="btn-facebook mr-15 mb-20 text-white" onClick={() => this.props.signinUserWithFacebook(this.props.history)}>
                      <i className="zmdi zmdi-facebook"></i>
                    </Button>
                    <Button variant="fab" className="btn-google mr-15 mb-20 text-white" onClick={() => this.props.signinUserWithGoogle(this.props.history)}>
                      <i className="zmdi zmdi-google"></i>
                    </Button>
                    <Button variant="fab" className="btn-twitter mr-15 mb-20 text-white" onClick={() => this.props.signinUserWithTwitter(this.props.history)}>
                      <i className="zmdi zmdi-twitter"></i>
                    </Button>
                    <Button variant="fab" className="btn-instagram mr-15 mb-20 text-white" onClick={() => this.props.signinUserWithGithub(this.props.history)}>
                      <i className="zmdi zmdi-github-alt"></i>
                    </Button>
                    <p className="text-muted">By signing up you agree to {AppConfig.brandName}</p>
                    <p className="mb-0"><a target="_blank" href="#/terms-condition" className="text-muted">Terms of Service</a></p> */}
                </div>
              </div>
              {/* <div className="col-sm-6 col-md-6 col-lg-6">
                  <SessionSlider />
                </div> */}

              <div className="rct-session-wrapper col-sm-6 col-md-6 col-lg-6" >
              <div className="login-right-div">
                <img src={uassess} width="75px" height="105px" className="uassess"/>
                <h1 className="welcome">Welcome!</h1>
                <hr className="line"></hr>
                <p className="welcome-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                  sed do eiusmod tem incididunt ut labore</p>
              </div>
             </div>
              {/* </div> */}

            </div>
          </div>
        </div>
      </QueueAnim>
    );
  }
}

// map state to props
const mapStateToProps = ({ authUser }) => {
  const { user, loading } = authUser;
  return { user, loading }
}

export default connect(mapStateToProps, {
  signinUserInFirebase,
  signinUserWithFacebook,
  signinUserWithGoogle,
  signinUserWithGithub,
  signinUserWithTwitter
})(Signin);
