import React, { Component } from 'react';
import { Card, CardBody } from 'reactstrap';
import 'antd/dist/antd.css';
import { Progress } from 'antd';
import Doughnut from '../charts/react-chartjs2/doughnut';


// intl messages
import IntlMessages from '../../util/IntlMessages';

import RctCollapsibleCard from '../../components/RctCollapsibleCard/RctCollapsibleCard';

import MUIDataTable from "mui-datatables";

import { Table, Input, Button, Icon } from 'antd';

const data = [{
  key: '1',
  no: '1',
  name: 'John Brown',
  age: 32,
  address: 'New York No. 1 Lake Park',
}, {
  key: '2',
  no: '2',
  name: 'Joe Black',
  age: 42,
  address: 'London No. 1 Lake Park',
}, {
  key: '3',
  no: '3',
  name: 'Jim Green',
  age: 32,
  address: 'Sidney No. 1 Lake Park',
}, {
  key: '4',
  no: '4',
  name: 'Jim Red',
  age: 32,
  address: 'London No. 2 Lake Park',
}, {
  key: '5',
  no: '5',
  name: 'Jim Red',
  age: 32,
  address: 'London No. 2 Lake Park',
}, {
  key: '6',
  no: '6',
  name: 'Jim Red',
  age: 32,
  address: 'London No. 2 Lake Park',
}, {
  key: '7',
  no: '7',
  name: 'Jim Red',
  age: 32,
  address: 'London No. 3 Lake Park',
}
];


class Assessment extends Component {
  state = {
    searchText: '',
  };

  handleSearch = (selectedKeys, confirm) => () => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  }

  handleReset = clearFilters => () => {
    clearFilters();
    this.setState({ searchText: '' });
  }


  render() {
    const columns = [{
      title: 'No',
      dataIndex: 'no',
      key: 'no',
    },{
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
        <div className="custom-filter-dropdown">
          <Input
            ref={ele => this.searchInput = ele}
            placeholder="Search name"
            value={selectedKeys[0]}
            onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
            onPressEnter={this.handleSearch(selectedKeys, confirm)}
          />
          <Button type="primary" onClick={this.handleSearch(selectedKeys, confirm)}>Search</Button>
          <Button onClick={this.handleReset(clearFilters)}>Reset</Button>
        </div>
      ),
      filterIcon: filtered => <Icon type="search" style={{ color: filtered ? '#108ee9' : '#aaa' }} />,
      onFilter: (value, record) => record.name.toLowerCase().includes(value.toLowerCase()),
      onFilterDropdownVisibleChange: (visible) => {
        if (visible) {
          setTimeout(() => {
            this.searchInput.focus();
          });
        }
      },
      render: (text) => {
        const { searchText } = this.state;
        return searchText ? (
          <span>
            {text.split(new RegExp(`(?<=${searchText})|(?=${searchText})`, 'i')).map((fragment, i) => (
              fragment.toLowerCase() === searchText.toLowerCase()
                ? <span key={i} className="highlight">{fragment}</span> : fragment // eslint-disable-line
            ))}
          </span>
        ) : text;
      },
    }, {
      title: 'Age',
      dataIndex: 'age',
      key: 'age',
    }, {
      title: 'Address',
      dataIndex: 'address',
      key: 'address',
      filters: [{
        text: 'London',
        value: 'London',
      }, {
        text: 'New York',
        value: 'New York',
      }],
      onFilter: (value, record) => record.address.indexOf(value) === 0,
    }];
    const icon = 'ti-arrow-right';
     return (
      <div className ='row'>
        <div className="col-md-12">
          <p><span aria-hidden="true" className={icon}></span>&nbsp;Insigths / Career Anchors</p>
        </div>
        <div className='row' style={{width:"100%",marginRight: "0",marginLeft: "0"}}>
          <div className='col-sm-6 col-md-4 col-xl-4 d-xs-half-block'>
            <Card className="rct-block">
              <CardBody className="d-flex py-15">
                <div className="mr-15 w-40 d-flex align-items-center">
                  <Progress type="circle" percent={30} width={80} />
                </div>
                <div>
                  <p className="mb-0"><IntlMessages id="components.spaceUsed" /></p>
                  <p className="text-danger font-3x mb-0">30<sub className="text-dark font-lg">/50GB</sub></p>
                  <Button color="primary" className="btn-xs"><IntlMessages id="widgets.buyMore" /></Button>
                </div>
              </CardBody>
            </Card>
          </div>
          <div className='col-sm-6 col-md-4 col-xl-4 d-xs-half-block'>
            <Card className="rct-block">
              <CardBody className="d-flex py-15">
                <div className="mr-15 w-40 d-flex align-items-center">
                  <Progress type="circle" percent={30} width={80} />
                </div>
                <div>
                  <p className="mb-0"><IntlMessages id="components.spaceUsed" /></p>
                  <p className="text-danger font-3x mb-0">30<sub className="text-dark font-lg">/50GB</sub></p>
                  <Button color="primary" className="btn-xs"><IntlMessages id="widgets.buyMore" /></Button>
                </div>
              </CardBody>
            </Card>
          </div>
          <div className='col-sm-6 col-md-4 col-xl-4 d-xs-half-block'>
            <Card className="rct-block">
              <CardBody className="d-flex py-15">
                <div className="mr-15 w-40 d-flex align-items-center">
                  <Progress type="circle" percent={100} width={80} />
                </div>
                <div>
                  <p className="mb-0"><IntlMessages id="components.spaceUsed" /></p>
                  <p className="text-danger font-3x mb-0">30<sub className="text-dark font-lg">/50GB</sub></p>
                  <Button color="primary" className="btn-xs"><IntlMessages id="widgets.buyMore" /></Button>
                </div>
              </CardBody>
            </Card>
          </div>
        </div>
        <div className='row' style={{width:"100%",marginRight:"0" ,marginLeft: "0"}}>
          <div className ='col-md-5' style={{display:"block"}}>
            <p>Career Anchors</p>
            <div className='uassess-style-box'>
              <div className='uassess-box-title'>
              <Card className="rct-block">
                <RctCollapsibleCard>
                                <Doughnut />
                        </RctCollapsibleCard>
                <div className="uassess-box-content-bottom">
                  <div className='uassess-full-width'>
                      <div className='uassess-fh'>Total Assessments</div>
                      <div className='uassess-sh'>:</div>
                      <div className='uassess-th'>5.9 avg</div>
                  </div>
                  <div className='uassess-full-width'>
                      <div className='uassess-fh'>Period of assessment</div>
                      <div className='uassess-sh'>:</div>
                      <div className='uassess-th'>5.9 avg</div>
                  </div>
                  <div className='uassess-full-width'>
                    <div className='uassess-fh'>Expiry date</div>
                      <div className='uassess-sh'>:</div>
                      <div className='uassess-th'>5.9 avg</div>
                  </div>
                </div>
                </Card>

              </div>
            </div>
          </div>
          <div className ='col-md-7'>
            <RctCollapsibleCard fullBlock>
            <Table columns={columns} dataSource={data} />;
            </RctCollapsibleCard>
          </div>
        </div>
      </div>
     )
  }
}
export default Assessment;