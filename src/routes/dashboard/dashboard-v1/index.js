/**
 * Dashboard V1
 */
import React, { Component } from 'react';

// Componets
import DailySales from '../../../components/Widgets/DailySales';
import ToDoListWidget from '../../../components/Widgets/ToDoList';
import CampaignPerformance from '../../../components/Widgets/CampaignPerformance';

import SupportRequest from '../../../components/Widgets/SupportRequest';
import NewCustomersWidget from '../../../components/Widgets/NewCustomers';
import Notifications from '../../../components/Widgets/Notifications';

import UserProfile from '../../../components/Widgets/UserProfile';
import QuoteOFTheDay from '../../../components/Widgets/QuoteOfTheDay';
import WeatherWidgetV2 from '../../../components/Widgets/WeatherV2';

import NewEmailsWidget from '../../../components/Widgets/NewEmails';
import EmployeePayrollWidget from '../../../components/Widgets/EmployeePayroll';

import ProjectManagement from '../../../components/Widgets/ProjectManagement';
import ProjectTaskManagement from '../../../components/Widgets/ProjectTaskManagement';
import LatestPost from '../../../components/Widgets/LatestPost';
import ActivityBoard from '../../../components/Widgets/ActivityBoard';

import TrafficChannel from '../../../components/Widgets/TrafficChannel';
import ActiveUser from '../../../components/Widgets/ActiveUser';
import PersonalSchedule from '../../../components/Widgets/PersonalSchedule';

import Space from '../../../components/Widgets/Space';
import FollowersWidget from '../../../components/Widgets/Followers';
import BookingInfo from '../../../components/Widgets/BookingInfo';
import NewOrderCountdown from '../../../components/Widgets/NewOrderCountdown';

import StockExchange from '../../../components/Widgets/StockExchange';
import TwitterFeeds from '../../../components/Widgets/TwitterFeeds';
import OurLocations from '../../../components/Widgets/OurLocations';

import BlogLayoutOne from '../../../components/Widgets/BlogLayoutOne';
import BlogLayoutTwo from '../../../components/Widgets/BlogLayoutTwo';
import BlogLayoutThree from '../../../components/Widgets/BlogLayoutThree';

import ShareFriends from '../../../components/Widgets/ShareFriends';
import PromoCoupons from '../../../components/Widgets/PromoCoupons';
import Rating from '../../../components/Widgets/Rating';

// page title bar
import PageTitleBar from '../../../components/PageTitleBar/PageTitleBar';

// rct card box
import { RctCard, RctCardContent } from '../../../components/RctCard';

// rct collapsible card
import RctCollapsibleCard from '../../../components/RctCollapsibleCard/RctCollapsibleCard';

// intl messages
import IntlMessages from '../../../util/IntlMessages';

// widgets data
import {
	todoData,
	newCustomers,
	messages,
	notificationTypes,
	notifications,
	ordersStatus,
	employeePayroll,
	newEmails,
	trafficStatus,
	totalSales,
	netProfit,
	expenses,
	totalEarns,
	taxStats
} from './data';

import {
	dailySales,
	trafficChannel,
	spaceUsed
} from '../../widgets/data';

const Dashboard = ({ match }) => (
	<div className="dashboard-v1">
		<PageTitleBar title={<IntlMessages id="sidebar.dashboard" />} match={match} />
		<div className="row">
			<RctCollapsibleCard
				customClasses="overflow-hidden"
				colClasses="col-sm-12 col-md-4 col-lg-4 w-xs-half-block"
				heading={<IntlMessages id="widgets.dailySales" />}
				badge={{
					name: <IntlMessages id="widgets.today" />,
					class: 'danger'
				}}
				collapsible
				reloadable
				closeable
				fullBlock
			>
				<DailySales
					label={dailySales.label}
					chartdata={dailySales.chartdata}
					labels={dailySales.labels}
				/>
			</RctCollapsibleCard>
			<RctCollapsibleCard
				colClasses="col-sm-12 col-md-4 col-lg-4 w-xs-half-block"
				heading={<IntlMessages id="widgets.trafficChannel" />}
				customClasses="overflow-hidden"
				badge={{
					name: <IntlMessages id="widgets.today" />,
					class: 'danger'
				}}
				collapsible
				reloadable
				closeable
				fullBlock
			>
				<TrafficChannel
					label={trafficChannel.label}
					chartdata={trafficChannel.chartdata}
					labels={trafficChannel.labels}
				/>
			</RctCollapsibleCard>
			<RctCollapsibleCard
				colClasses="col-sm-12 col-md-4 col-lg-4 w-xs-full"
				customClasses="overflow-hidden"
				heading={<IntlMessages id="widgets.campaignPerformance" />}
				collapsible
				reloadable
				closeable
			>
				<CampaignPerformance />
			</RctCollapsibleCard>
		</div>
		<div className="row">
			<RctCollapsibleCard
				customClasses="to-do-list tour-step-2"
				colClasses="col-sm-12 col-md-4 col-lg-4 d-xs-half-block"
				heading={<IntlMessages id="widgets.toDoList" />}
				collapsible
				reloadable
				closeable
				fullBlock
			>
				<ToDoListWidget data={todoData} />
			</RctCollapsibleCard>
			<RctCollapsibleCard
				colClasses="col-sm-12 col-md-4 col-lg-4 d-xs-half-block"
				heading={<IntlMessages id="widgets.usersList" />}
				badge={{
					name: <IntlMessages id="widgets.lastWeek" />,
					class: 'info'
				}}
				fullBlock
				collapsible
				reloadable
				closeable
			>
				<NewCustomersWidget data={newCustomers} />
			</RctCollapsibleCard>
			<RctCollapsibleCard
				colClasses="col-sm-12 col-md-4 col-lg-4 d-xs-full"
				fullBlock
			>
				<Notifications
					messages={messages}
					notificationTypes={notificationTypes}
					notifications={notifications}
				/>
			</RctCollapsibleCard>
		</div>
		<div className="row">
			<RctCollapsibleCard
				colClasses="col-sm-6 col-md-4 col-lg-4 w-8-half-block"
				fullBlock
			>
				<UserProfile />
			</RctCollapsibleCard>
			<RctCollapsibleCard
				colClasses="col-sm-6 col-md-4 col-lg-4 w-8-half-block"
				heading={<IntlMessages id="widgets.quoteOfTheDay" />}
				customClasses="review-slider overflow-hidden bg-primary text-white"
			>
				<QuoteOFTheDay />
			</RctCollapsibleCard>
			<RctCollapsibleCard
				colClasses="col-sm-6 col-md-4 col-lg-4 w-8-full"
				fullBlock
			>
				<WeatherWidgetV2 />
			</RctCollapsibleCard>
		</div>
		<div className="row">
			<RctCollapsibleCard
				colClasses="col-sm-12 col-md-7 col-xl-7 b-100 w-xs-full"
				heading={<IntlMessages id="widgets.newEmails" />}
				collapsible
				reloadable
				closeable
				fullBlock
			>
				<NewEmailsWidget data={newEmails} />
			</RctCollapsibleCard>
			<RctCollapsibleCard
				colClasses="col-sm-12 col-md-5 col-xl-5 b-100 w-xs-full"
				heading={<IntlMessages id="widgets.employeePayroll" />}
				collapsible
				reloadable
				closeable
				fullBlock
			>
				<EmployeePayrollWidget data={employeePayroll} />
			</RctCollapsibleCard>
		</div>
		<div className="row">
			<RctCollapsibleCard
				colClasses="col-sm-12 col-md-12 col-lg-6 d-xxs-full"
				heading={<IntlMessages id="widgets.projectManagement" />}
				collapsible
				reloadable
				closeable
				fullBlock
				customClasses="overflow-hidden"
				badge={{
					name: <IntlMessages id="widgets.weekly" />,
					class: 'success'
				}}
			>
				<ProjectManagement />
			</RctCollapsibleCard>
			<RctCollapsibleCard
				colClasses="col-sm-12 col-md-12 col-lg-6 d-xxs-full"
				heading={<IntlMessages id="widgets.projectTaskManagement" />}
				collapsible
				reloadable
				closeable
				fullBlock
				customClasses="overflow-hidden"
				badge={{
					name: <IntlMessages id="widgets.weekly" />,
					class: 'danger'
				}}
			>
				<ProjectTaskManagement />
			</RctCollapsibleCard>
		</div>
		<div className="row">
			<RctCollapsibleCard
				colClasses="col-sm-12 col-md-12 col-lg-6 d-xxs-full"
				heading={<IntlMessages id="widgets.latestPost" />}
				collapsible
				reloadable
				closeable
				customClasses="overflow-hidden"
				fullBlock
			>
				<LatestPost />
			</RctCollapsibleCard>
			<RctCollapsibleCard
				colClasses="col-sm-12 col-md-12 col-lg-6 d-xxs-full"
				heading={<IntlMessages id="widgets.activityBoard" />}
				collapsible
				reloadable
				closeable
				customClasses="overflow-hidden"
				fullBlock
			>
				<ActivityBoard />
			</RctCollapsibleCard>
		</div>
		<div className="row">
			<RctCollapsibleCard
				colClasses="col-sm-12 col-md-4 col-lg-4 w-8-half-block"
				heading={<IntlMessages id="widgets.supportRequest" />}
				collapsible
				reloadable
				closeable
				fullBlock
				customClasses="overflow-hidden"
			>
				<SupportRequest />
			</RctCollapsibleCard>
			<RctCollapsibleCard
				colClasses="col-sm-12 col-md-4 col-lg-4 w-8-half-block"
				fullBlock
				customClasses="overflow-hidden"
			>
				<ActiveUser />
			</RctCollapsibleCard>
			<RctCollapsibleCard
				colClasses="col-sm-12 col-md-4 col-lg-4 w-8-full"
				fullBlock
				customClasses="overflow-hidden bg-light-yellow"
			>
				<PersonalSchedule />
			</RctCollapsibleCard>
		</div>
		<div className="row">
			<div className="col-sm-6 col-md-3 col-xl-3 d-xs-half-block">
				<Space data={spaceUsed} />
			</div>
			<div className="col-sm-6 col-md-3 col-xl-3 d-xs-half-block">
				<NewOrderCountdown />
			</div>
			<div className="col-sm-6 col-md-3 col-xl-3 d-xs-half-block">
				<FollowersWidget />
			</div>
			<div className="col-sm-6 col-md-3 col-xl-3 d-xs-half-block">
				<BookingInfo />
			</div>
		</div>
		<div className="row">
			<RctCollapsibleCard
				colClasses="col-sm-6 col-md-4 col-lg-4 w-8-half-block"
				heading={<IntlMessages id="widgets.stockExchange" />}
				collapsible
				reloadable
				closeable
				fullBlock
				customClasses="overflow-hidden"
			>
				<StockExchange />
			</RctCollapsibleCard>
			<RctCollapsibleCard
				colClasses="col-sm-6 col-md-4 col-lg-4 w-8-half-block"
				fullBlock
			>
				<TwitterFeeds />
			</RctCollapsibleCard>
			<RctCollapsibleCard
				heading={<IntlMessages id="widgets.ourLocations" />}
				colClasses="col-sm-6 col-md-4 col-lg-4 w-8-full"
				collapsible
				reloadable
				closeable
				fullBlock
			>
				<OurLocations />
			</RctCollapsibleCard>
		</div>
		<div className="row">
			<div className="col-sm-6 col-md-4 col-lg-4 w-8-half-block"
			>
				<BlogLayoutOne />
			</div>
			<div className="col-sm-6 col-md-4 col-lg-4 w-8-half-block">
				<BlogLayoutTwo />
			</div>
			<div className="col-sm-6 col-md-4 col-lg-4 w-8-full">
				<BlogLayoutThree />
			</div>
		</div>
		<div className="row">
			<div className="col-sm-12 col-md-6 col-xl-6 w-8-full">
				<ShareFriends />
			</div>
			<div className="col-sm-12 col-md-6 col-xl-6 w-8-full">
				<PromoCoupons />
			</div>
			<div className="col-sm-12 col-md-6 col-xl-6 w-8-full">
				<Rating />
			</div>
		</div>
	</div>
);

export default Dashboard;